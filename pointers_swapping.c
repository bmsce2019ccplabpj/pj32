#include<stdio.h>
int swap(int *x ,int *y)
{
    int temp;
    temp=*x;
    *x=*y;
    *y=temp;
}
int main ()
{
    int x,y;
    printf("ENTER THE NUMBERS TO BE SWAPPED");
    scanf("%d%d",&x,&y);
    swap(&x,&y);
    printf("THE NUMBERS AFTER SWAPPING IS %d AND %d",x,y);
    return 0;
}
